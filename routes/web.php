<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LandController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('layouts.main');
});*/

Route::view('/', 'layouts.main');
//Route::resource('lands', LandController::class);
Route::get('/lands', [LandController::class, 'index'])->name('lands.index');
Route::get('/lands/enregistrer-un-pays', [LandController::class, 'create'])->name('lands.creer');
Route::post('/lands', [LandController::class, 'store'])->name('lands.stock');
Route::get('/lands/afficher/{id}', [LandController::class, 'show'])->name('lands.afficher');
Route::get('/lands/modifier/{id}', [LandController::class, 'edit'])->name('lands.modifier');
Route::post('/lands/supprimer/{id}', [LandController::class, 'destroy'])->name('lands.supprimer');
Route::post('/lands/mise-a-jour/{id}', [LandController::class, 'update'])->name('lands.maj');






/*Route::get('/', function () {
    return view('welcome');
});*/
