@extends('layouts.main')

@section('contenu')
    <div class="row m-lg-3">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Formulaire de création</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="{{ route('lands.stock') }}" method="POST">
                    @csrf
                    @method('POST')
                    <div class="card-body">
                        <div class="form-group">
                            <label for="libelle">Libelle</label>
                            <input type="text" class="form-control" name="libelle" placeholder="Entrez un pays">
                        </div>
                        <div class="form-group">
                            <label for="capitale">Capitale</label>
                            <input type="text" class="form-control" name="capitale" placeholder="Entrez la capitale">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" class="form-control" name="description"
                                placeholder="Entrez une description">
                        </div>
                        <div class="form-group">
                            <label for="code_indicatif">Code indicatif</label>
                            <input type="text" class="form-control" name="code_indicatif"
                                placeholder="Entrez le code indicatif du pays">
                        </div>
                        <div class="form-group">
                            <label for="population">Population</label>
                            <input type="text" class="form-control" name="population"
                                placeholder="Entrez le nombre d'habitants">
                        </div>
                        <div class="form-group">
                            <label for="superficie">Superficie</label>
                            <input type="text" class="form-control" name="superficie"
                                placeholder="Entrez la superficie">
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <!-- select -->
                                <div class="form-group">
                                    <label>Continent</label>
                                    <select class="form-control" name="continent">
                                        <option value="" selected hidden>Choisissez un continent</option>
                                        <option value="Afrique">Afrique</option>
                                        <option value="Asie">Asie</option>
                                        <option value="Antartique">Antartique</option>
                                        <option value="Amérique">Amérique</option>
                                        <option value="Europe">Europe</option>
                                        <option value="Océanie">Océanie</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Monnaie nationale</label>
                                    <select class="form-control" name="monnaie">
                                        <option value="" selected hidden>Choisissez une monnaie</option>
                                        <option value="EUR">Euro</option>
                                        <option value="DOLLAR">Dollar</option>
                                        <option value="XOF">Franc</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Langue parlée</label>
                                    <select class="form-control" name="langue">
                                        <option value="" selected hidden>Choisissez une langue</option>
                                        <option value="FR">Français</option>
                                        <option value="EN">Anglais</option>
                                        <option value="AR">Arabe</option>
                                        <option value="ES">Espagnol</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Laique ou pas</label>
                                    <select class="form-control" name="est_laique">
                                        <option value="" selected hidden>Choississez</option>
                                        <option value="1">Laique</option>
                                        <option value="0">Non laique</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Sauvegarder</button>
                    </div>
                </form>
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection
