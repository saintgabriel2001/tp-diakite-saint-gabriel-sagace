@extends('layouts.main')

@section('contenu')
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Liste des pays</h3>
                <div class="card-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <a href="{{ route('lands.creer') }}">
                            <button type="button" class="btn btn-block btn-danger">Nouveau pays</button>
                        </a>
                    </div>
                </div>

            </div>
            <!-- /.card-header -->
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                    <thead>
                        <tr>
                            <th>Libelle</th>
                            <th class="w-5">Description</th>
                            <th>Code indicatif</th>
                            <th>Continent</th>
                            <th>Population</th>
                            <th>Capitale</th>
                            <th>Monnaie</th>
                            <th>Langue parlée</th>
                            <th>Superficie</th>
                            <th>Laique</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($lands as $pays)
                            <tr>
                                <td>{{ $pays->libelle }}</td>
                                <td class="text-wrap">{{ $pays->description }}</td>
                                <td>{{ $pays->code_indicatif }}</td>
                                <td>{{ $pays->continent }}</td>
                                <td>{{ $pays->population }}</td>
                                <td>{{ $pays->capitale }}</td>
                                <td>{{ $pays->monnaie }}</td>
                                <td>{{ $pays->langue }}</td>
                                <td>{{ $pays->superficie }} Km²</td>
                                <td>{{ $pays->est_laique }}</td>
                                <td>
                                    <a href="{{ route('lands.afficher', ["id" => $pays->id]) }}">
                                        <button class="btn btn-info btn-circle" type="button">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                        <a href="{{ route('lands.modifier', ["id" => $pays->id]) }}">
                                            <button class="btn btn-primary btn-circle" type="button">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        </a>
                                        <a href="{{ route('lands.supprimer', ["id" => $pays->id]) }}">
                                            <button class="btn btn-danger btn-circle" type="button">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </a>
                                    </a>
                                </td>
                            </tr>
                         
                        @endforelse
                    </tbody>

                </table>

            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>


@endsection
