<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Land extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        //
    }
}
