<?php

namespace App\Http\Controllers;

use App\Models\Land;
use Illuminate\Http\Request;

class LandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lands = Land::all();
        return view('Pages.Lands.index', [
            'lands' => Land::Paginate(5)
        ], compact('lands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Pages.Lands.creer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "libelle" => "required|unique:lands,libelle",
            "code_indicatif" => "required",
            "continent" => "required|string",
            "capitale" => "required|string",
            "population" => "required|numeric",
            "superficie" => "required|numeric",
            "monnaie" => "required",
            "langue" => "required",
            "est_laique" => "required"
        ]);

        //Enregistrement
        /*Land::create([
            "libelle" => $request->get("libelle"),
            "description" => $request->get("description"),
            "continent" => $request->get("continent"),
            "code_indicatif" => $request->get("code_indicatif"),
            "capitale" => $request->get("capitale"),
            "population" => $request->get("population"),
            "monnaie" => $request->get("monnaie"),
            "langue" => $request->get("langue"),
            "superficie" => $request->get("superficie"),
            "est_laique" => $request->get("est_laique"),
        ]);*/
        Land::create($request->all());
        return redirect()->route('lands.index')->with('success','Pays enregistré avec succès.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Land  $land
     * @return \Illuminate\Http\Response
     */
    public function show(Land $land)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lands = Land::findOrFail($id);
        return view("Pages.Lands.modif", ["lands" => $lands]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            "libelle" => "required|unique:lands,libelle",
            "code_indicatif" => "required",
            "continent" => "required|string",
            "description" => "required",
            "capitale" => "required|string",
            "population" => "required|numeric",
            "superficie" => "required|numeric",
            "monnaie" => "required",
            "langue" => "required",
            "est_laique" => "required"
        ]);

        $land = Land::find($id);

        $land->libelle = $request->libelle;
        $land->description = $request->description;
        $land->code_indicatif = $request->code_indicatif;
        $land->continent = $request->continent;
        $land->capitale = $request->capitale;
        $land->population = $request->population;
        $land->superficie = $request->superficie;
        $land->monnaie = $request->monnaie;
        $land->langue = $request->langue;
        $land->est_laique = $request->est_laique;

        //dd($request);
        $land->update();

        //$land->save($request->all());
        return redirect()->route('lands.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Land::find($id)->delete();
        return redirect()->route('lands.index');
    }
}
