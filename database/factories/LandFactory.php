<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Land>
 */
class LandFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $djai = ['XOF', 'EUR', 'DOLLAR'];
        $cont = ['Afrique', 'Europe', 'Amérique', 'Océanie', 'Asie', 'Antarctique'];
        $lang = ['FR', 'EN', 'AR', 'ES'];

        return [

            'libelle' => $this->faker->country(),
            'description'=> $this->faker->paragraph($nbSentences = 1, $variableNbSentences = true),
            'code_indicatif' => $this->faker->numerify('+ ###'),
            'continent' => $cont[random_int(0, 5)],
            'population' => rand(10000, 999999),
            'capitale' => $this->faker->city(),
            'monnaie' => $djai[random_int(0, 2)],
            'langue' => $lang[random_int(0, 3)],
            'superficie' => rand(1000000, 99999999),
            "est_laique"=> $this->faker->boolean(),




        ];
    }
}
